ViewModelArrayUtil={
	addData:function(datas,newdata){
		datas.push(newdata);
	},
	addDatas:function(datas,newdatas){
		var that=this;
		newdatas.forEach(function(newdata){
			that.addData(datas,newdata);
		});
	},
	resetDatas:function(datas,newdatas){
		datas.removeAll();
		this.addDatas(datas,newdatas);
	},
	getDataByIndex:function(datas,index){
		return datas()[index];
	},
	removeData:function(datas,rmdata){
		datas.remove(rmdata);
	},
	removeByIndex:function(datas,index){
		var rmdata=datas()[index];
		datas.remove(rmdata);
	},
	removeAll:function(datas,rmdatas){
		datas.removeAll(rmdatas);
	},
	clear:function(datas){
		datas.removeAll();
	}
}
UIUtils={
	adaptTableTH:function(tableid) {
	    $("#"+tableid).children("thead").find("td,th").each(function () {
	        var idx = $(this).index();
	        var td = $(this).closest("#"+tableid).children("tbody").children("tr:first").children("td,th").eq(idx);
	        $(this).width(td.width());
	    })
	},
	refeshTableCBox:function(tableRow,value){
		var checkbox = tableRow.childNodes[cboxIndex];
		checkbox.firstChild.checked=value;
	}
}
NetworkUtils={
	callService:function(type,url,data,successCallback){
		$.ajax({
	        type:type,
	        url: url,
	        data:data,
	        dataType: "json",
	        timeout: 30000,
	        success: function (result) {
	        	if(result){
	        		if(result.statuscode=="0"&&result.datas){
		        		var datas=result["datas"];
		        		successCallback(datas);
		        	}else if(result.errinfo){
		        		alert("系统错误："+result.errinfo);
		        	}
	        	}else{
	        		alert("访问服务器未返回结果");
	        	}
	        },
	        error: function(){
	        	alert("网络访问异常");
	        }
	    });
	}
}