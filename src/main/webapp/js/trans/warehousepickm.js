/**
 * Created by lincw on 2017/12/04.
 */

//当前选中备料申请单
var selectedPickmBill = {
    selectRows: new Array()
};

function adaptTH() {
    $("table").children("thead").find("th").each(function () {
        var idx = $(this).index();
        var td = $(this).closest("table").children("tbody").children("tr:first").children("td").eq(idx);
        $(this).width(td.width());
    })
};

$(function () {
    adaptTH();
    refreshList();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target; // 激活的标签页
        e.relatedTarget; // 前一个激活的标签页
        adaptTH();
    })
});

//刷新列表
function refreshList() {
    getPickmBillList();
}

//根据查询到的数据构造备料申请单
function constructPickmList(msg) {
    var rows = msg.rows;
    if (null == rows || rows.length == 0) {
        return;
    }
    //清空
    var tbody = $("#req tbody");
    tbody.empty();
    //重新构造列表

    var result = "";
    for (var x in rows) {
        if (x % 2 == 1) {
            result += "<tr class='success' "
        } else {
            result += "<tr "
        }
//        var batchcodeMsg = rows[x].batchcode;
        result += " onclick='rowClick(this)'>";
        result += "<td class='req_checkbox'>" +
            "<div class='billid' style='display: none'>" + rows[x].billid + "</div>" +
            "<div class='rowno' style='display: none'>" + rows[x].rowno + "</div>" +
            "<div class='reqwh' style='display: none'>" + rows[x].destpos + "</div>" +
            "<input type='checkbox'  disabled='disabled' value=''/></td>";
        result += "<td class='vbillcode'>" + rows[x].vbillcode + "</td>";
        result += "<td class='position'>" + rows[x].destposText + "</td>";
        result += "<td class='provpos'>" + rows[x].provposText + "</td>";
        result += "<td class='matname'>" + rows[x].matname + " / " + rows[x].matspec + " / " + rows[x].mattype + "</td>";
//        result += "<td class='matBatch'>" 
//        	   + "<div class='form-group'>"
//               + "<div class='col-sm-10'>"
//               + "<select class='batchcode' id='batchcode"+rows[x].vbillcode+rows[x].rowno+"' name='batchcode' onclick='batchcodeChange()' type='text'>";
//        result +="<option value=' '></option>";
//        for(var id in batchcodeMsg){
//        result +="<option value='"+batchcodeMsg[id].pk_batchcode+"'>"+batchcodeMsg[id].batchcode+"</option>";
//        }
//        result +="</select>"
//               + "</div>"
//               + "</div>" 
//        	   + "</td>";
        result += "<td class='reqnum'><div class='nnum'>" + rows[x].reqnum + "</div><div class='matunit'>" + rows[x].matunit + "</div></td>";
        result += "<td class='nassnum'><div class='asnum'>" + rows[x].naccstockoutastnum + "</div><div class='matunit'>" + rows[x].matunit + "</div></td>";
        result += "<td class='npenum'><div class='penum'>" + rows[x].naccpendingastnum + "</div><div class='matunit'>" + rows[x].matunit + "</div></td>";
        result += "<td class='plannum'><input class='pnum' id='pnum"+rows[x].vbillcode+rows[x].rowno+"' type='number' value=''/><div class='matunit'>" + rows[x].matunit + "</div></td>";
        result += "<td class='status'>" + "未运送" + "</td>";
        result += "</tr>"
    }
    tbody.append(result);
    adaptTH();
}

//行点击处理
function rowClick(tablerow) {
    var col = tablerow.childNodes;
    var rowIndex = tablerow.rowIndex - 1;
    var checkbox = col[0];
    //点击行过滤，一次只能选中同一个仓库同一站点
    //如果没有其它选中行,存入仓库站点
    if (selectedPickmBill.selectRows.length == 0) {
    	selectedPickmBill.selectRows.push(rowIndex);
        checkbox.lastChild.checked = true;
    } else {
        //如果点击行未被选中，则判断仓库站点是否一致
        if (!checkbox.lastChild.checked) {
        	checkbox.lastChild.checked = true;
            selectedPickmBill.selectRows.push(rowIndex);
        }
        //如果点击行被选中，弃选该行
        else {
            checkbox.lastChild.checked = false;
            for (var i = 0; i < selectedPickmBill.selectRows.length; i++) {
                if (selectedPickmBill.selectRows[i] == rowIndex) {
                	selectedPickmBill.selectRows.splice(i, 1);
                    break;
                }
            }
        }
    }
}


//生产其他出库单后台操作
function SaveTransBill() {
    if (selectedPickmBill.selectRows.length == 0) {
        $("#hint").text("请选择备料申请单！");
        setTimeout("$('#hint').text('')",3000);
        return;
    }
    var transInfo = {
        rows: ""
    }

    var rows = new Array();
    //alert("length="+selectedTransBill.selectRows.length);
    //var tbody = $("#zk").children("tbody");
    var tbody = $("#req tbody")
    for (var x in selectedPickmBill.selectRows) {
    	var tr = tbody[0].children[selectedPickmBill.selectRows[x]];
    	//alert(tr.children[0].firstChild.nextSibling.innerText);
        var info = {
            billid: tr.children[0].firstChild.innerText,
            rowno: tr.children[0].firstChild.nextSibling.innerText,
            reqwh: tr.children[0].children[2].innerText,
            matinfo: tr.children[4].innerText,
//            pk_batchcode: $('#batchcode'+tr.children[0].firstChild.innerText+tr.children[0].firstChild.nextSibling.innerText).val(),
//            batchcode: $('#batchcode'+tr.children[0].firstChild.innerText+tr.children[0].firstChild.nextSibling.innerText).find("option:selected").text(),
            reqnum: tr.children[5].firstChild.innerText,
            nassnum: tr.children[6].firstChild.innerText,
            npenum: tr.children[7].firstChild.innerText,
            plannum: $('#pnum'+tr.children[1].innerText+tr.children[0].firstChild.nextSibling.innerText).val(),
            matunit: tr.children[5].lastChild.innerText
        }
        //alert(info.billid);
        rows.push(info);
    }
    //alert("success3");
    transInfo.rows = rows;
    SaveTransBillRequest(transInfo);
}

function SaveTransBillSuccess(msg) {
    if (msg.errorinfo == null) {
    	selectedPickmBill.selectRows.splice(0, selectedPickmBill.length);
        alert("生成转库任务成功");
    }else{
    	alert(msg.error);
    }
}

function ToTransBill(){
	//跳转到转库单仓库终端
    window.location.href = "WareHouse";
}