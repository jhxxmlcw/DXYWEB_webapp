/**
 * Created by yijx on 2017/4/1.
 */

//保存转库单请求
function saveBillRequst(info) {
    $.ajax({
        type:"post",
        url:"saveTransBill",
        data:{info:JSON.stringify(info)},
        traditional:true,
        dataType:"json",
        timeout:30000,
        berforesend:function (XMLHttpRequest) {},
        success:function (msg) {
            billSaved(msg);
        }
    });
}

//查询货位请求
function searchAllocation() {
    $.ajax({
        type: "post",
        url: "getAllocation",
        data: "",
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            gotAllocation(msg);
        }
    });
}

//查询已发车转库单
function reqDoneTransBill(info) {
    $.ajax({
        type: "post",
        url: "getDoneTransBill",
        data: {rows:JSON.stringify(info)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            gotDoneTransBill(msg);
        }
    });
}

//打印转库单
function printTransBill(info) {
    $.ajax({
        type: "post",
        url: "printTransBill",
        data: {info:JSON.stringify(info)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            transBillPrinted(msg);
        }
    });
}

//通知发满车
function dispatch(tbcodes) {
    $.ajax({
        type: "post",
        url: "dispatchTruck",
        data: {"tbcodes":JSON.stringify(tbcodes)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            truckDispatched(msg);
        }
    });
}