/**
 * Created by yijx on 2017/4/1.
 * 数字时钟
 */
$(document).ready(function() {
// 建立月份与星期的数组，便于调用
    var monthNames = [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ];
    var dayNames= ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"]

// 创建一个日期对象
    var newDate = new Date();
// 根据日期对象延申出当前时间
    newDate.setDate(newDate.getDate());
// 输出年月日
    $('#Date').html( newDate.getFullYear()+ "年 " +  monthNames[newDate.getMonth()] + ' ' +newDate.getDate() + '日 '  + dayNames[newDate.getDay()] );

    setInterval( function() {
        //创建时间对象，并取得当前时间的秒数值
        var seconds = new Date().getSeconds();
        // 如果秒为个位数则在前面加个0
        $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
    },1000);

    setInterval( function() {
        // 取得当前时间的分钟数值
        var minutes = new Date().getMinutes();
        // 分钟数小于10则在前面加上0
        $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
    },1000);

    setInterval( function() {
        // 取得当前时间的小时数值
        var hours = new Date().getHours();
        // 如果小时数值小于10则在前面加个0
        $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
    }, 1000);

});