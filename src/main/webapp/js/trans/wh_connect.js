/**
 * Created by yijx on 2017/4/11.
 */

//刷新备料申请列表请求
function refreshRequest() {
    $.ajax({
        type: "post",
        url: "refreshList",
        data: "",
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            constructList(msg);
        },
        erroe: function (msg) {
            alert(msg);
        }
    });
}

//获取转库单列表
function getTransBillList() {
    $.ajax({
        type: "post",
        url: "getTransBillList",
        data: "",
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            constructTransList(msg);
        },
        erroe: function (msg) {
            alert(msg);
        }
    });
}

//获取备料申请单列表
function getPickmBillList() {
    $.ajax({
        type: "post",
        url: "getPickmBillList",
        data: "",
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            constructPickmList(msg);
        },
        erroe: function (msg) {
            alert(msg);
        }
    });
}


//生产库存其他出库单后台操作
function toOtherOutTransBillRequest(transInfo) {
    $.ajax({
        type: "post",
        url: "toOtherOutTransBill",
        traditional: true,
        data: {info: JSON.stringify(transInfo)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
        	toOtherOutTransBillSuccess(msg);
        },
        error: function (msg) {
            alert(msg);
        }
    });
}

//生成转库单的后台操作
function SaveTransBillRequest(transInfo) {
    $.ajax({
        type: "post",
        url: "SaveTransBill",
        traditional: true,
        data: {info: JSON.stringify(transInfo)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
        	SaveTransBillSuccess(msg);
        },
        error: function (msg) {
            alert(msg);
        }
    });
}