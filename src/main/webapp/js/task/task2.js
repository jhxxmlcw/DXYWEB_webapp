var viewmodel=new (function(){
	this.taskdatas=ko.observableArray([]);
	this.pickdatas=ko.observableArray([]);
})();
$(function () {
	getTaskPickDatas();
	ko.applyBindings(viewmodel);
	UIUtils.adaptTableTH("tasktable");
	mask();
});
function getTaskPickDatas(){
	$.ajax({
        type: "post",
        url: "taskQuery",
        async: false,
        dataType: "json",
        timeout: 30000,
        success: function (msg) {
        	ViewModelArrayUtil.resetDatas(viewmodel.taskdatas,msg.datas.tasks);
        },
        error: function(msg){
        	alert(msg);
        }
    });
}
//遮罩处理
function mask() {
    jQuery(document).ready(function($) {
        $('.theme-login').click(function(){
            $('.theme-popover-mask').fadeIn(100);
            $('.theme-popover').slideDown(200);
        })
        $('.theme-poptit .close').click(function(){
            $('.theme-popover-mask').fadeOut(100);
            $('.theme-popover').slideUp(200);
        })
    })
}
function rowClick(tableRow) {
	var rowIndex = tableRow.rowIndex-1;
	var clickedTask=ViewModelArrayUtil.getDataByIndex(viewmodel.taskdatas,rowIndex);
	clickedTask.selected=!clickedTask.selected;
	var isselected=clickedTask.selected;
	UIUtils.refeshTableCBox(tableRow,0,isselected);
	if(isselected){
		var reqpickm={
				matinfo:clickedTask.matinfo,
				matbatch:clickedTask.matbatch,
				reqNum:clickedTask.reqNum,
				lasttime:clickedTask.starttime,
				transNum:clickedTask.reqNum,
				status:clickedTask.status
		};
		ViewModelArrayUtil.addData(viewmodel.pickdatas,reqpickm);
	}else{
		var selectedPickm=null;
		for(var i=0;i<viewmodel.pickdatas().length;i++){
			if(clickedTask.matinfo.pk==viewmodel.pickdatas()[i].matinfo.pk&&clickedTask.matbatch==viewmodel.pickdatas()[i].matbatch){
				selectedPickm=viewmodel.pickdatas()[i];
			}
		}
		if(selectedPickm){
			ViewModelArrayUtil.removeData(viewmodel.pickdatas,selectedPickm);
		}
	}
}
//任务列表行点击处理
function rowClick2(tableRow) {
    var col =tableRow.childNodes;
    var checkbox = col[0];
    if(checkbox.children.length <=2){
        return;
    }
    //如果checkbox未被选中，则选中，加入到被选中任务数组
    if(!checkbox.firstChild.checked){
        checkbox.firstChild.checked = true;
        var matCode = checkbox.lastChild.innerText;
        var matBatch=tableRow.childNodes[5].innerText;
        var reqNum = tableRow.childNodes[6].childNodes[0].innerText;
        var task= {
            rowNo:tableRow.rowIndex-1,
            matCode:matCode,
            matBatch:matBatch,
            reqNum:reqNum
        };
        selectedTask.push(task);
        var hasMat = false;
        var matRow = -1;
        //检查备料申请行，是否已有相同物料
        for(var x in editReqpickm){
            if(editReqpickm[x].matCode == matCode && editReqpickm[x].matBatch == matBatch){
                hasMat = true;
                matRow = editReqpickm[x].editRowNo;
                break;
            }
        }
        if(hasMat){
            //更新行需求重量
            updateReqNum(matRow,reqNum);
            for(var i in editReqpickm){
                if(editReqpickm[i].matCode == matCode && editReqpickm[x].matBatch == matBatch){
                    editReqpickm[i].reqNum = addReqNum(editReqpickm[i].reqNum,reqNum);
                }
            }
        }
        else {
            var reqPickm = constructReqPickm(tableRow);
            //如果没有，则添加到需求物料汇总editReqpickm
            //备料申请表汇总编辑
            var editRowNo = addReqPickm([reqPickm]);
            var reqEditRow = {
                editRowNo:editRowNo,
                matCode :matCode,
                matBatch:matBatch,
                reqNum:reqPickm.reqnum,
                reqTime:reqPickm.reqtime
            }
            editReqpickm.push(reqEditRow);
        }
    }
    //如果checkbox已被选中，则弃选，正在编辑的备料计划需求重量减去该行任务需求重量
    else{
        checkbox.firstChild.checked = false;
        //从被选中任务数组中删除
        var rowIndex = tableRow.rowIndex-1;
        var operateTask=null;
        for(var i=0;i<selectedTask.length;i++){
            if(selectedTask[i].rowNo ==  rowIndex){
                operateTask =selectedTask[i];
                selectedTask.splice(i,1);
                break;
            }
        }
        var index = -1;
        //备料申请减去相应需求重量，若重量<=0，删除该备料申请行
        for(var i=0;i<editReqpickm.length;i++){
            if(operateTask.matCode == editReqpickm[i].matCode && operateTask.matBatch ==editReqpickm[i].matBatch){
                if(editReqpickm[i].reqNum - operateTask.reqNum<=0){
                    index = editReqpickm[i].editRowNo;
                    deleteEditRow(index);
                    editReqpickm.splice(i,1);
                }else{
                    editReqpickm[i].reqNum -= operateTask.reqNum;
                    updateReqNum(editReqpickm[i].editRowNo,0-operateTask.reqNum);
                }
                break;
            }
        }
        if(index !=-1){
            for(var x in editReqpickm){
                if(editReqpickm[x].editRowNo >index){
                    editReqpickm[x].editRowNo -=1;
                }
            }
        }
    }
    adaptTH();
}
