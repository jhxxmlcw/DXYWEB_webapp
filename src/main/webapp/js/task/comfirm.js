/**
 * Created by lincw on 2017/12/04.
 */

//当前选中转库单
var selectedTransBill = {
    selectRows: new Array()
};

function adaptTH() {
    $("table").children("thead").find("th").each(function () {
        var idx = $(this).index();
        var td = $(this).closest("table").children("tbody").children("tr:first").children("td").eq(idx);
        $(this).width(td.width());
    })
};

$(function () {
    adaptTH();
    refreshList();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target; // 激活的标签页
        e.relatedTarget; // 前一个激活的标签页
        adaptTH();
    })
});

//刷新列表
function refreshList() {
    getTransBillList();
}

//根据查询到的数据构造转库单
function constructTransList(msg) {
    var rows = msg.rows;
    if (null == rows || rows.length == 0) {
        return;
    }
    //清空
    var tbody = $("#zk tbody");
    tbody.empty();
    //重新构造列表

    var result = "";
    for (var x in rows) {
        if (x % 2 == 1) {
            result += "<tr class='success' "
        } else {
            result += "<tr "
        }
        var batchcodeMsg = rows[x].batchcode;
        result += " onclick='rowClick(this)'>";
        result += "<td class='zk_checkbox'>" +
            "<div class='billid' style='display: none'>" + rows[x].vbillcode + "</div>" +
            "<div class='rowno' style='display: none'>" + rows[x].rowno + "</div>" +
            "<div class='reqwh' style='display: none'>" + rows[x].destpos + "</div>" +
            "<input type='checkbox'  disabled='disabled' value=''/></td>";
        result += "<td class='vbillcode'>" + rows[x].vbillcode + "</td>";
        result += "<td class='position'>" + rows[x].destposText + "</td>";
        result += "<td class='provpos'>" + rows[x].provposText + "</td>";
        result += "<td class='matname'>" + rows[x].matinfo.matname + " / " + rows[x].matinfo.matspec + " / " + rows[x].matinfo.mattype + "</td>";
        result += "<td class='matBatch'>" 
        	   + "<div class='form-group'>"
               + "<div class='col-sm-10'>"
               + "<select class='batchcode' id='batchcode"+rows[x].vbillcode+rows[x].rowno+"' name='batchcode' onclick='batchcodeChange()' type='text'>";
        result +="<option value=' '></option>";
        for(var id in batchcodeMsg){
        result +="<option value='"+batchcodeMsg[id].pk_batchcode+"'>"+batchcodeMsg[id].batchcode+"</option>";
        }
        result +="</select>"
               + "</div>"
               + "</div>" 
        	   + "</td>";
        result += "<td class='reqnum'><div class='nnum'>" + rows[x].nnum + "</div><div class='matunit'>" + rows[x].matinfo.dw + "</div></td>";
        result += "<td class='getnum'><input class='gnum' id='gnum"+rows[x].vbillcode+rows[x].rowno+"' type='number' value=''/><div class='matunit'>" + rows[x].matinfo.dw + "</div></td>";
        result += "<td class='status'>" + "未确认接收" + "</td>";
        result += "</tr>"
    }
    tbody.append(result);
    adaptTH();
}

//行点击处理
function rowClick(tablerow) {
    var col = tablerow.childNodes;
    var rowIndex = tablerow.rowIndex - 1;
    var checkbox = col[0];
    //点击行过滤，一次只能选中同一个仓库同一站点
    //如果没有其它选中行,存入仓库站点
    if (selectedTransBill.selectRows.length == 0) {
    	selectedTransBill.selectRows.push(rowIndex);
        checkbox.lastChild.checked = true;
    } else {
        //如果点击行未被选中，则判断仓库站点是否一致
        if (!checkbox.lastChild.checked) {
        	checkbox.lastChild.checked = true;
            selectedTransBill.selectRows.push(rowIndex);
        }
        //如果点击行被选中，弃选该行
        else {
            checkbox.lastChild.checked = false;
            for (var i = 0; i < selectedTransBill.selectRows.length; i++) {
                if (selectedTransBill.selectRows[i] == rowIndex) {
                	selectedTransBill.selectRows.splice(i, 1);
                    break;
                }
            }
        }
    }
}


//生产其他入库单后台操作
function toOtherInTransBill() {
    if (selectedTransBill.selectRows.length == 0) {
        $("#hint").text("请选择转库单！");
        setTimeout("$('#hint').text('')",3000);
        return;
    }
    var transInfo = {
        rows: ""
    }

    var rows = new Array();
    //alert("length="+selectedTransBill.selectRows.length);
    //var tbody = $("#zk").children("tbody");
    var tbody = $("#zk tbody")
    for (var x in selectedTransBill.selectRows) {
    	var tr = tbody[0].children[selectedTransBill.selectRows[x]];
    	//alert(tr.children[0].firstChild.nextSibling.innerText);
        var info = {
            billid: tr.children[0].firstChild.innerText,
            rowno: tr.children[0].firstChild.nextSibling.innerText,
            reqwh: tr.children[0].children[2].innerText,
            matinfo: tr.children[4].innerText,
            pk_batchcode: $('#batchcode'+tr.children[0].firstChild.innerText+tr.children[0].firstChild.nextSibling.innerText).val(),
            batchcode: $('#batchcode'+tr.children[0].firstChild.innerText+tr.children[0].firstChild.nextSibling.innerText).find("option:selected").text(),
            reqnum: tr.children[6].firstChild.innerText,
            getnum: $('#gnum'+tr.children[0].firstChild.innerText+tr.children[0].firstChild.nextSibling.innerText).val(),
            matunit: tr.children[6].lastChild.innerText
        }
        //alert(info.billid);
        rows.push(info);
    }
    //alert("success3");
    transInfo.rows = rows;
    toOtherInTransBillRequest(transInfo);
}

function toOtherInTransBillSuccess(msg) {
    if (msg.errinfo == null) {
    	selectedTransBill.selectRows.splice(0, selectedTransBill.length);
        alert("生成其他入库单成功");
    }else{
    	alert(msg.error);
    }
}