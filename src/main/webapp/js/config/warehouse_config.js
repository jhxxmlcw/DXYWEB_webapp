/**
 * Created by yijx on 2017/4/6.
 */
var propInfo;
var itemKey=["org","warehouse"];
$(function () {
    propInfo = {
        username: $('#username').val(),
        pk_org:$.cookie("org")==null?"":$.cookie("org"),
        orgText: $.cookie("orgText")==null?"":$.cookie("orgText"),
        warehouse: $.cookie("warehouse")==null?"":$.cookie("warehouse"),
        warehouseText:$.cookie("warehouseText")==null?"":$.cookie("warehouseText"),
    };
    var html = "<option value='"+propInfo.pk_org+"'>"+propInfo.orgText+"</option>";
    $('#org').append(html);
    html = "<option value='"+propInfo.warehouse+"'>"+propInfo.warehouseText+"</option>";
    $('#warehouse').append(html);
    
    //切换组织后，仓库站点置空,重新获取仓库信息
    $("#org").change(function () {
        $('#warehouse').html("");
        $('#warehouse').val("");
        if($('#org').val()==""){
            alert("必须先选择组织！");
            return;
        }
        var org = $('#org').val();
        $.ajax({
            type:"post",
            url:"searchWarehouse",
            data:{pk_org:org},
            dataType:"json",
            timeout:30000,
            beforesend:function (XMLHttpRequest) {
            },
            success:function (msg) {
            	gotWarehouse(msg);
            }
        });
    });
});

// 保存
function saveConfig() {
    propInfo.username = $('#username').val();
    propInfo.pk_org=$("#org").val();
    propInfo.warehouse=$('#warehouse').val();
    propInfo.orgText=$("#org").find("option:selected").text();
    propInfo.warehouseText = $('#warehouse').find("option:selected").text();
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }
    $.ajax({
        type: "post",
        url: "positionToSession",
        data: propInfo,
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                // 保存成功，设置所有input不能编辑
                for (var key in itemKey) {
                    $("#" + itemKey[key]).attr('disabled', true);
                }
                // 隐藏保存按钮放开编辑按钮
                document.getElementById("saveBtn").style.visibility = "hidden";
                document.getElementById("editBtn").style.visibility = "visible";
                $.cookie("org",propInfo.pk_org,{expires:7});
                $.cookie("warehouse",propInfo.warehouse,{expires:7});
                $.cookie("orgText",propInfo.orgText,{expires:7});
                $.cookie("warehouseText",propInfo.warehouseText,{expires:7});
                $.cookie("destposes",propInfo.destposes,{expires:7});
            }
        }
    });
}
// 确定信息，进入工作界面
function confirmConfig() {
    for (var key in itemKey) {
        // 开始前先保存
        if (!$("#" + itemKey[key]).attr("disabled")) {
            alert("请先保存配置信息！");
            return;
        }
    }
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }

    $.ajax({
        type: "post",
        url: "positionToSession",
        data: propInfo,
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                //各项配置信息存入cookie
                $.cookie("org",propInfo.pk_org,{expires:7});
                $.cookie("warehouse",propInfo.warehouse,{expires:7});
                $.cookie("orgText",propInfo.orgText,{expires:7});
                $.cookie("warehouseText",propInfo.warehouseText,{expires:7});
                $.cookie("destposes",propInfo.destposes,{expires:7});
                
                //跳转到备料申请仓库终端
                window.location.href = "WareHousePickm";
            }
        }
    });
}

// 修改
function editConfig() {
	//获取组织
    $.ajax({
        type:"post",
        url:"searchOrg",
        data:"",
        dataType:"json",
        timeout:30000,
        beforesend:function (XMLHttpRequest) {
        },
        success:function (msg) {
            gotOrg(msg);
        }
    });
    for (var key in itemKey) {
    	$("#" + itemKey[key]).attr("disabled",false);
    }
    // 隐藏编辑按钮放开保存按钮
    document.getElementById("saveBtn").style.visibility = "visible";
    document.getElementById("editBtn").style.visibility = "hidden";
}

//后台查询出仓库后界面响应
function gotWarehouse(msg) {
    if(null !=msg>0){
        var warehouse = $("#warehouse").empty();
        warehouse.append("<option value=''></option>");
        for(var id in msg){
            warehouse.append("<option value='"+msg[id].warehouse+"'>"+msg[id].warehouseText+"</option>")
        }
    }
}

//后台查询出组织后界面响应
function gotOrg(msg) {
    if(null !=msg && msg.length>0){
        var org = $("#org").empty();
        org.append("<option value=' '></option>");
        for(var id in msg){
            org.append("<option value='"+msg[id].pk_org+"'>"+msg[id].orgText+"</option>")
        }
        var orgid=$.cookie("org");
        var orgname=$.cookie("orgText");
        if(orgid&&orgname){
        	$("#org").val(orgid);
//        	$("#org").find("option[text='"+orgname+"']").attr
        }
        
    }
}