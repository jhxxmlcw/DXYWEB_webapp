/**
 * Created by yijx on 2017/4/6.
 */
var propInfo;
//var destpos;
//var provpos;
var itemKey=["org","destpos"];
$(function () {
    propInfo = {
        username: $('#username').val(),
        pk_org:$.cookie("org")==null?"":$.cookie("org"),
        orgText: $.cookie("orgText")==null?"":$.cookie("orgText"),
        destpos: $.cookie("destpos")==null?"":$.cookie("destpos"),
        destposText:$.cookie("destposText")==null?"":$.cookie("destposText"),
//        destpos:$.cookie("destpos")==null?"":$.cookie("destpos"),
//        destposText:$.cookie("destposText")==null?"":$.cookie("destposText"),
//        provpos:$.cookie("provpos")==null?"":$.cookie("provpos"),
//        provposText:$.cookie("provposText")==null?"":$.cookie("provposText"),
    };
    var html = "<option value='"+propInfo.pk_org+"'>"+propInfo.orgText+"</option>";
    $('#org').append(html);
    html = "<option value='"+propInfo.destpos+"'>"+propInfo.destposText+"</option>";
    $('#destpos').append(html);
//    $('#provpos').val(propInfo.provposText);
//    $('#destpos').val(propInfo.destposText);
    
    //切换组织后，获取仓库信息
    $("#org").change(function () {
        $('#position').html("");
        $('#position').val("");
        if($('#org').val()==""){
            alert("必须先选择组织！");
            return;
        }
        var org = $('#org').val();
        $.ajax({
            type:"post",
            url:"searchWarehouse",
            data:{pk_org:org},
            dataType:"json",
            timeout:30000,
            beforesend:function (XMLHttpRequest) {
            },
            success:function (msg) {
            	gotDestpos(msg);
            }
        });
    });
    
//  //切换站点后，重置destpos,provpos
//    $("#position").change(function () {
//    	$('#provpos').val("");
//    	$('#destpos').val("");
//    	destpos = undefined;
//    	provpos = undefined;
//    	if($('#position').val()==""){
//            alert("必须先选择站点！");
//            return;
//        }
//        var position = $('#position').val();
//        $.ajax({
//            type:"post",
//            url:"searchDespost",
//            data:{position:position},
//            dataType:"json",
//            timeout:30000,
//            beforesend:function (XMLHttpRequest) {
//            },
//            success:function (msg) {
//                gotDestpos(msg);
//            }
//        });
//    });
});

// 保存
function saveConfig() {
    propInfo.username = $('#username').val();
    propInfo.pk_org=$("#org").val();
    propInfo.destpos=$('#destpos').val();
//    propInfo.provposText = $('#provpos').val();
    propInfo.orgText=$("#org").find("option:selected").text();
    propInfo.destposText = $('#destpos').find("option:selected").text();
//    propInfo.destposText = $('#destpos').val();
//    propInfo.provpos = provpos;
//    propInfo.destpos = destpos;
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }
    $.ajax({
        type: "post",
        url: "positionToSession",
        data: propInfo,
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                // 保存成功，设置所有input不能编辑
                for (var key in itemKey) {
                    $("#" + itemKey[key]).attr('disabled', true);
                }
                // 隐藏保存按钮放开编辑按钮
                document.getElementById("saveBtn").style.visibility = "hidden";
                document.getElementById("editBtn").style.visibility = "visible";
                $.cookie("org",propInfo.pk_org,{expires:7});
//                $.cookie("provpos",propInfo.provpos,{expires:7});
//                $.cookie("position",propInfo.position,{expires:7});
                $.cookie("destpos",propInfo.destpos,{expires:7});
                $.cookie("orgText",propInfo.orgText,{expires:7});
                $.cookie("destposText",propInfo.positionText,{expires:7});
//                $.cookie("destposText",propInfo.destposText,{expires:7});
//                $.cookie("provposText",propInfo.provposText,{expires:7});
            }
        }
    });
}
// 确定信息，进入工作界面
function confirmConfig() {
    for (var key in itemKey) {
        // 开始前先保存
        if (!$("#" + itemKey[key]).attr("disabled")) {
            alert("请先保存配置信息！");
            return;
        }
    }
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }

    $.ajax({
        type: "post",
        url: "positionToSession",
        data: propInfo,
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                //各项配置信息存入cookie
                $.cookie("org",propInfo.pk_org,{expires:7});
//                $.cookie("position",propInfo.position,{expires:7});
//                $.cookie("provpos",propInfo.provpos,{expires:7});
                $.cookie("destpos",propInfo.destpos,{expires:7});
                $.cookie("orgText",propInfo.orgText,{expires:7});
//                $.cookie("positionText",propInfo.positionText,{expires:7});
                $.cookie("destposText",propInfo.destposText,{expires:7});
//                $.cookie("provposText",propInfo.provposText,{expires:7});
//                //跳转到车间终端
//                window.location.href = "WorkTask";
                //跳转到领料确认终端
                window.location.href = "LoadComfirmTrans";
//                if(dataMsg.usertype == "workshop"){
//                    window.location.href = "WorkTask";
//                }else if(dataMsg.usertype =="warehouse"){
//                    window.location.href = "WareHouse";
//                }

            }
        }
    });
}

// 修改
function editConfig() {
	//获取组织
    $.ajax({
        type:"post",
        url:"searchOrg",
        data:"",
        dataType:"json",
        timeout:30000,
        beforesend:function (XMLHttpRequest) {
        },
        success:function (msg) {
            gotOrg(msg);
        }
    });
    for (var key in itemKey) {
        $("#" + itemKey[key]).attr("disabled",false);
    }
    // 隐藏编辑按钮放开保存按钮
    document.getElementById("saveBtn").style.visibility = "visible";
    document.getElementById("editBtn").style.visibility = "hidden";
}

//后台查询出站点后界面响应
function gotDestpos(msg) {
	//alert(msg);
	if(null !=msg){
        var destpos = $("#destpos").empty();
        destpos.append("<option value=''></option>");
        for(var id in msg){
        	destpos.append("<option value='"+msg[id].warehouse+"'>"+msg[id].warehouseText+"</option>")
        }
    }
}

////后台查询出暂存区和供应仓后界面响应
//function gotDestpos(msg) {
//	if(null !=msg){
//		$("#provpos").val(msg[0].name); //设置
//    	$("#destpos").val(msg[1].name); //设置
//    	destpos = msg[1].pk;
//    	provpos = msg[0].pk;
//    }
//}

//后台查询出组织后界面响应
function gotOrg(msg) {
    if(null !=msg && msg.length>0){
        var org = $("#org").empty();
        org.append("<option value=' '></option>");
        for(var id in msg){
            org.append("<option value='"+msg[id].pk_org+"'>"+msg[id].orgText+"</option>")
        }
        var orgid=$.cookie("org");
        var orgname=$.cookie("orgText");
        if(orgid&&orgname){
        	$("#org").val(orgid);
//        	$("#org").find("option[text='"+orgname+"']").attr
        }
        
    }
}