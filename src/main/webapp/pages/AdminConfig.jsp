<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport"
          content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/config.css"/>

    <title>配置界面</title>
</head>


<body>
<div class="header">服务配置</div>
<%
    if (session.getAttribute("username") == null) {
%>
<h1>未登录！</h1>
3秒后跳转到登录页面
<p>
    如果没有跳转，请点<a href="Login">这里</a>
</p>
<%
        response.setHeader("refresh", "3;URL=Login");
        return;
    }
%>
<div class="top">NC服务配置</div>
<div class="section">
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">NC地址:</span>
            <input id="ncip" name="ncip" type="text" class="form-control" value="${config.ncip}" readonly="readonly">
        </div>
        <div class="input-group" style="float: right; width: 49%">
            <span class="input-group-addon ">NC数据源:</span>
            <input type="text" id="ncdataresource" name="ncdataresource" class="form-control" value="${config.ncdataresource}" readonly="readonly">
        </div>
    </div>
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">NC用户:</span>
            <input id="ncusername" name="ncusername" type="text" class="form-control" value="${config.ncusername}" readonly="readonly">
        </div>
        <div class="input-group" style="float: right; width: 49%">
            <span class="input-group-addon ">用户密码:</span>
            <input readonly="readonly" id="ncuserpassword" name="ncuserpassword" type="password" class="form-control"  value="${config.ncuserpassword}">
        </div>
    </div>
    <!-- 
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">NC版本:</span>
            <input readonly="readonly" id="ncversion" name="ncversion" type="text" class="form-control" value="${config.ncversion}">
        </div>
    </div>
     -->
    <div class="top">MES服务配置</div>
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">MES地址:</span>
            <input id="mesip" name="mesip" type="text" class="form-control" value="${config.mesip}" readonly="readonly">
        </div>
        <div class="input-group" style="float: right; width: 49%">
            <span class="input-group-addon ">MES数据源:</span>
            <input type="text" id="mesdataresource" name="mesdataresource" class="form-control" value="${config.mesdataresource}" readonly="readonly">
        </div>
    </div>
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">MES用户:</span>
            <input id="mesusername" name="mesusername" type="text" class="form-control" value="${config.mesusername}" readonly="readonly">
        </div>
        <div class="input-group" style="float: right; width: 49%">
            <span class="input-group-addon ">用户密码:</span>
            <input readonly="readonly" id="mesuserpassword" name="mesuserpassword" type="password" class="form-control"  value="${config.mesuserpassword}">
        </div>
    </div>
    <!-- 
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">MES版本:</span>
            <input readonly="readonly" id="mesversion" name="mesversion" type="text" class="form-control" value="${config.mesversion}">
        </div>
    </div>
     -->
    <!-- <button id="changePwd">修改密码</button> -->
    <div style="text-align: center;">
    <button id="linkNC" onclick="linkNC()">测试NC连接</button>
    <button id="linkMES" onclick="linkMES()">测试MES连接</button>
    <button id="edit" onclick="edit()">修改</button>
    <button id="save" onclick="save()" style="display: none;">保存</button>
    <button id="goLogin" onclick="goLogin()">返回登录页</button>
    </div>
    <%--<div class="property" style="background-color: #ffffff">--%>
        <%--<label id="editAdminPsd" class="item-ttl ttlnote">修改管理员密码</label>--%>
    <%--</div>--%>
    <div class="property" style="background-color: #84C329; height: 30px">
    </div>
</div>
<script type="text/javascript" src="js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="js/config/admin_config.js"></script>
</body>

</html>