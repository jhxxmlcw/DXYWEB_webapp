<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>物料转库</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link href="css/trans_custom.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid">
    <%
        if (session.getAttribute("username") == null) {
    %>
    <h1>未登录！</h1>
    3秒后跳转到登录页面
    <p>
        如果没有跳转，请点<a href="Login">这里</a>
    </p>
    <%
            response.setHeader("refresh", "3;URL=Login");
            return;
        }
    %>
    <div class="span12">
        <div class="theme-buy">
            <a class="btn btn-primary btn-large theme-login" href="javascript:;" id="maskin" style="display: none">弹出遮罩</a>
        </div>
        <div class="theme-popover">
            <div class="theme-poptit">
                <a href="javascript:void(0);" title="关闭" class="close">×</a>
                <h4 class="text-center">录入重量</h4>
            </div>
            <div class="theme-popbod dform"></div>
        </div>
        <div class="theme-popover-mask"></div>
    </div>

    <h4 class="text-center">转库单</h4>
    <div class="row-fluid">
        <div class="section">
            <div class="property">
                <div class="input-group" style="float: left; width: 49%">
                    <span class="input-group-addon ">出库仓库:</span>
                    <input id="outwarehouse" name="outwarehouse" type="text" class="form-control"
                           value="${outwarehouse}" readonly="readonly">
                </div>
                <div class="input-group" style="float: right; width: 49%">
                    <span class="input-group-addon ">入库仓库:</span>
                    <input type="text" id="inwarehouse" name="inwarehouse" class="form-control"
                           value="${inwarehouse}" readonly="readonly">
                </div>
            </div>
            <div class="property">
                <div class="input-group" style="float: left; width: 49%">
                    <span class="input-group-addon ">发出站点:</span>
                    <input id="outposition" name="outposition" type="text" class="form-control"
                           value="${outposition}" readonly="readonly">
                </div>
                <div class="input-group" style="float: right; width: 49%">
                    <span class="input-group-addon ">接收站点:</span>
                    <input readonly="readonly" id="inposition" name="inposition" type="text"
                           class="form-control" value="${inposition}">
                </div>
            </div>
        </div>
        <div class="tabbable" id="tabs-238921">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#panel-720674" data-toggle="tab" id="addbill">转库</a>
                </li>
                <li>
                    <a href="#panel-547607" data-toggle="tab" id="donebill">已发车</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="panel-720674">
                    <div class="row-fluid">
                        <div class="span12">
                            <table class="table table-bordered" id="req">
                                <thead>
                                <tr>
                                    <th>转库单号</th>
                                    <th>物料名称/规格/型号</th>
                                    <th>批次号</th>
                                    <th>发料货位</th>
                                    <th>需求重量</th>
                                    <th>累计转库</th>
                                    <th>待转库</th>
                                    <th>装车重量</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${rows}" var="row">
                                    <tr>
                                        <td class="transHead">
                                            <div class="transBillcode"></div>
                                            <div class="transRowNo" style="display: none"></div>
                                            <div class="reqid" style="display: none">${row.billid}</div>
                                            <div class="rowno" style="display: none">${row.rowno}</div>
                                        </td>
                                        <td class="matname">${row.matinfo}</td>
                                        <td class="matBatch">${row.matBatch}</td>
                                        <td class="allocation"><select type="text">
                                        </select></td>
                                        <td class="reqnum"><div class="matnum">${row.reqnum}</div><div class="matunit">${row.matunit}</div></td>
                                        <td class="donenum"><div class="matnum">${row.donenum}</div><div class="matunit">${row.matunit}</div></td>
                                        <td class="waitnum"><div class="matnum">${row.reqnum-row.donenum>0?row.reqnum-row.donenum:0}</div><div class="matunit">${row.matunit}</div></td>
                                        <td class="trucknum">
                                            <input type="text" onclick="turckNumClick(this)">
                                            <div class="matunit">${row.matunit}</div>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <span6 id="hint"></span6>
                        <span6 id="transbtn">
                            <button class="btn btn-info btn-mini" type="button" id="back" onclick="javascript:history.back(-1)">返回</button>
                            <button class="btn btn-info btn-mini" type="button" id="reqtruck">要空车</button>
                            <button class="btn btn-info btn-mini" type="button" id="savebill">保存</button>
                            <button class="btn btn-info btn-mini" type="button" id="printbill">打印</button>
                            <button class="btn btn-info btn-mini" type="button" id="dispatch">发车</button>
                        </span6>
                    </div>
                </div>
                <div class="tab-pane" id="panel-547607">
                    <div class="row-fluid">
                        <div class="span12">
                            <table class="table table-bordered" id="done">
                                <thead>
                                <tr>
                                    <th>转库单号</th>
                                    <th>物料名称/规格/型号</th>
                                    <th>批次号</th>
                                    <th>发料货位</th>
                                    <th>需求重量</th>
                                    <th>装车重量</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="transHead" style="width: 100px"></td>
                                        <td class="matname" style="width: 150px"></td>
                                        <td class="matBatch" style="width: 100px"></td>
                                        <td class="allocation" style="width: 100px"></td>
                                        <td class="reqnum" style="width: 100px"></td>
                                        <td class="trucknum" style="width: 100px"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/trans/transbill.js"></script>
<script type="text/javascript" src="js/trans/tb_connect.js"></script>
</body>
</html>