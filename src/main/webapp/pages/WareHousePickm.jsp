<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>仓库发料</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link href="css/wh_custom.css" rel="stylesheet">
    <link href="css/clock.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid">
    <%
        if (session.getAttribute("username") == null) {
    %>
    <h1>未登录！</h1>
    3秒后跳转到登录页面
    <p>
        如果没有跳转，请点<a href="Login">这里</a>
    </p>
    <%
            response.setHeader("refresh", "3;URL=Login");
            return;
        }
    %>
    <h4 class="text-center">备料申请任务列表</h4>
    <div class="row-fluid">
        <div class="span6" id="refresh">
            <button class="btn btn-info btn-mini" type="button" id="reqrefresh" onclick="refreshList()">刷新</button>
        </div>
        <div class="span6" id="current" style="float: right">
            <div id="Date"></div>
            <ul class="clock">
                <li id="hours"></li>
                <li class="point">:</li>
                <li id="min"></li>
                <li class="point">:</li>
                <li id="sec"></li>
            </ul>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <table class="table table-bordered" id="req">
                <thead>
                <tr>
                    <th class="req_checkbox"></th>
                    <th>备料申请单号</th>
                    <th>需求站点</th>
                    <th>供应站点</th>
                    <th>物料名称/规格/型号</th>
                    <th>需求重量</th>
                    <th>已入库重量</th>
                    <th>待转运重量</th>
                    <th>本次转库重量</th>
                    <th>状态</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="req_checkbox"><input type='checkbox' value=''/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="matname"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row-fluid bottom-hint">
        <span6 id="hint"></span6>
        <span6 id="reqbtn">
        <button class="btn btn-info btn-mini" type="button" id="SaveTransbill" onclick="SaveTransBill()">生成转库任务</button>
        <button class="btn btn-info btn-mini" type="button" id="ToTransbill" onclick="ToTransBill()">转库单列表</button>
        </span6>
    </div>
</div>

<script type="text/javascript" src="js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/trans/warehousepickm.js"></script>
<script type="text/javascript" src="js/trans/wh_connect.js"></script>
<script type="text/javascript" src="js/trans/clock.js"></script>


</body>
</html>