package com.yonyou.mm.controller;

import com.yonyou.mm.aop.LogUtil;
import com.yonyou.mm.service.ComplexBusinessService;
import com.yonyou.mm.service.TransferService;
import com.yonyou.mm.util.ConstUtil;
import com.yonyou.mm.util.ExceptionUtils4DxyWeb;
import com.yonyou.mm.util.TestData;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Level;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by yijx on 2017/4/1.
 */
@Controller
public class TransferController {

    /**
     * 刷新备料申请列表，从后台查询数据
     * 前台参数：ConstUtil.WAREHOUSE，POSITION
     * 传NC参数：默认参数，ConstUtil.TASKTYPE
     * NC回传：'rows':[{'billid': '备料申请id','rowno':'备料申请汇总行号'," +
     "'cinstockid':'发料仓库','reqTime':'需求时间','position':'需求站点'," +
     "'matname':'物料名称'," + "'matspec':'物料规格','mattype':'物料型号','matBatch':'物料批次','reqNum':'需求数量'," +
     "'matunit':'物料单位'," + "'nacctransnum':'已转库数量','status':'状态'},{}]
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/getPickmBillList")
    public void refresh(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        JSONObject param = new JSONObject();
        
        //添加参数
        param.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));
        JSONObject result = new JSONObject();
		try {
			result = ComplexBusinessService
					.queryDestposByWarehouse(param, session);
			JSONArray destposes = new JSONArray();
			if(result==null){
				return;
			}else if(result.containsKey("datas")
					&& result.get("datas") instanceof JSONArray) {
				destposes = result.getJSONArray("datas");
			}
			//暂存区集合
			info.put("destposes",destposes);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
        //当前仓库
        info.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));
        
        //类型
        info.put(ConstUtil.TASKTYPE,"queryReqPickm");

        //查询结果
        result= TransferService.requestService(info,session);

//        JSONArray rows = JSONArray.fromObject(TestData.reqPickmList);
//        result.put("rows",rows);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }
    
    /**
     * 刷新转库单，从后台查询数据
     * 前台参数：ConstUtil.WAREHOUSE
     * 传NC参数：默认参数，ConstUtil.TASKTYPE
     */
    @RequestMapping(value = "/getTransBillList")
    public void getTransBillList(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
    	HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        //添加参数
        //当前组织
        info.put(ConstUtil.ORG,session.getAttribute(ConstUtil.ORG));
        //当前站点对应的供应仓
        info.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));
        //类型
        info.put(ConstUtil.TASKTYPE,"getTransBillList");

        //查询结果
        JSONObject result= TransferService.requestService(info,session);

        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }

    /**
     * 发送到转库单界面的数据存入Session
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/toTransBill")
    public void toTransBill(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = JSONObject.fromObject(paramsData.get("info").toString());
        session.setAttribute("transInfo",info);
        JSONObject result = new JSONObject();
        result.put("flag",0);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }

    }
    
    /**
     * 通过转库单生成其他出库单
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/toOtherOutTransBill")
    public void toOtherOutTransBill(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = JSONObject.fromObject(paramsData.get("info").toString());
        
        //当前组织
        info.put(ConstUtil.ORG,session.getAttribute(ConstUtil.ORG));
        //当前站点对应的供应仓
        info.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));
        //类型
        info.put(ConstUtil.TASKTYPE,"toOtherOutTransBill");
        //System.out.println(info.toString());
        JSONObject result = TransferService.requestService(info,session);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }

    }

    /**
     * 跳转到转库单界面
     * 前台参数：ConstUtil.WAREHOUSENAME(当前仓库)，POSITIONNAME(当前站点)，POSITION(接收站点)，inwarehouse(接收仓库)
     * transInfo:{billid:备料申请id,rowno:备料申请行号,matinfo:物料名称/规格/型号,
     * matBatch:物料批次,reqnum:需求数量,donenum: 已转库数量,matunit:物料单位}
     * @param request
     * @param response
     * @param paramsData
     * @param model
     * @return
     */
    @RequestMapping(value = "/transBillPage")
    public String transBillPage(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData, Model model){
        HttpSession session = request.getSession();
        if(session.getAttribute("transInfo") !=null) {
            JSONObject info = JSONObject.fromObject(session.getAttribute("transInfo"));
            JSONArray rows = info.getJSONArray("rows");
            model.addAttribute("outwarehouse", info.get(ConstUtil.PROVPOS));
            model.addAttribute("outposition", info.get(ConstUtil.PROVPOS));
            model.addAttribute("inposition", info.get(ConstUtil.POSITION));
            model.addAttribute("inwarehouse", info.get("inwarehouse"));
            model.addAttribute("rows", rows);
            session.removeAttribute("transInfo");
            return "TransferBill";
        }
        return "WareHouse";
    }

    /**
     * 查询货位
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAllocation")
    public void getAllocation(HttpServletRequest request, HttpServletResponse response){
        JSONObject allocation = JSONObject.fromObject(TestData.allocation);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(allocation.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }

    /**
     * 查询已发车的转库单
     * 前台参数：ConstUtil.WAREHOUSE,POSITION,
     * info:[{"billid":"备料申请id","rowno" :"备料申请汇总行号"},{}]
     *
     * 传NC参数：默认参数，ConstUtil.TASKTYPE,info
     *
     * 请求NC成功回传：{'rows':[{'billid':'备料申请id','rowno':'备料申请行号',
     * 'transBillCode':'转库单号','trasnRowNo':'转库单行号','truckNum':'装车重量'},{}]
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getDoneTransBill")
    public void getDoneTransBill(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        JSONArray rows = JSONArray.fromObject(request.getParameter("rows"));
        JSONObject info = new JSONObject();
        info.put(ConstUtil.ROWS,rows);
        info.put(ConstUtil.TASKTYPE,"getDoneTransBill");

        JSONObject result=  TransferService.requestService(info,session);
        JSONArray transBills = new JSONArray();
        if(result.containsKey(ConstUtil.ROWS) && result.get(ConstUtil.ROWS) instanceof JSONArray){
            transBills =result.getJSONArray(ConstUtil.ROWS);
        }
//        transBills = JSONArray.fromObject(TestData.transBill);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(transBills.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }


    /**
     * 打印转库单号、转库单行号等信息
     * @param request
     * @param response
     */
    @RequestMapping(value = "/printTransBill")
    public void printTransBill(HttpServletRequest request, HttpServletResponse response){

    }

    /**
     * 叫空车
     */
    @RequestMapping(value = "/reqTruck")
    public void reqTruck(HttpServletRequest request, HttpServletResponse response){
    	HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        info.put(ConstUtil.TASKTYPE, "getEmptyCar");
        info.put(ConstUtil.PROVPOS,session.getAttribute(ConstUtil.PROVPOS));
        info.put(ConstUtil.POSITION, session.getAttribute(ConstUtil.POSITION));
        JSONObject obj = TransferService.requestService(info,session);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(obj.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }

    /**
     * 发满车
     * @param request
     * @param response
     */
    @RequestMapping(value = "/dispatchTruck")
    public void dispatchTruck(HttpServletRequest request, HttpServletResponse response,@RequestParam HashMap<String,Object> paramsData){
    	HttpSession session = request.getSession();
    	JSONArray  zkdcodes =  JSONArray.fromObject(paramsData.get("tbcodes"));
        JSONObject info = new JSONObject();
        info.put(ConstUtil.TASKTYPE, "sendFullCar");
        info.put(ConstUtil.PROVPOS,session.getAttribute(ConstUtil.PROVPOS));
        info.put(ConstUtil.POSITION, session.getAttribute(ConstUtil.POSITION));
        info.put("zkdcodes", zkdcodes);
        JSONObject obj = TransferService.requestService(info,session);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(obj.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }
    
    /**
     * 后台备料申请推生产领料推转库单，如果有选中多个备料申请，则多张转库单
     * 前台参数：ConstUtil.WAREHOUSE,POSITION,
     * info:[{"billid":"备料申请id","rowno" :"备料申请汇总行号","truckNum":"装车数量"},{}]
     *
     * 传NC参数：默认参数，ConstUtil.TASKTYPE,ConstUtil.WAREHOUSE(当前仓库),ConstUtil.POSITION(当前站点),info
     *
     * 请求NC成功回传：{'rows':[{'billid':'备料申请id','rowno':'备料申请行号',
     * 'transBillCode':'转库单号','trasnRowNo':'转库单行号','truckNum':'装车重量'},{}]
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/SaveTransBill")
    public void saveTransBill(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = JSONObject.fromObject(paramsData.get("info").toString());
        info.put(ConstUtil.TASKTYPE,"saveTrans");
        //发出仓库
        info.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));

        JSONObject result = TransferService.requestService(info,session);

        try {
            PrintWriter pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
    }

}
