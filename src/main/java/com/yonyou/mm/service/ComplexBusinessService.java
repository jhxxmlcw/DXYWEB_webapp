package com.yonyou.mm.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.yonyou.mm.util.ConstUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 复杂业务服务提供类，为了处理需要同时访问nc和mes的业务
 * @author honglg
 *
 */
public class ComplexBusinessService {
	
	private static final String MES_ACTION_QUERYTASK="88001";
	private static final String MES_ACTION_QUERYWORKCENTER="88002";
	private static final String MES_ACTION_QUERYDESTPOS="88003";
	private static final String MES_ACTION_QUERYDESTPOSBYWH="88004";
	private static Map<String,JSONObject> STATUS_VALUE2OBJ=new HashMap<String,JSONObject>();
	static{
		JSONObject obj=new JSONObject();
		obj.put("value", "1");
		obj.put("name", "自由");
		STATUS_VALUE2OBJ.put("1",obj);
		obj=new JSONObject();
		obj.put("value", "2");
		obj.put("name", "下达");
		STATUS_VALUE2OBJ.put("2",obj);
		obj=new JSONObject();
		obj.put("value", "3");
		obj.put("name", "生产中");
		STATUS_VALUE2OBJ.put("3",obj);
		obj=new JSONObject();
		obj.put("value", "4");
		obj.put("name", "完成");
		STATUS_VALUE2OBJ.put("4",obj);
	};
	/**
	 * 获取机台指令和备料计划
	 * @param param
	 * @param session
	 * @return
	 */
	public static JSONObject queryTaskAndPicks(JSONObject param, HttpSession session) throws Exception{
		JSONObject result=new JSONObject();
		//先访问nc，根据站点获取它绑定的工作中心
		Object destpos = session.getAttribute(ConstUtil.DESTPOS);
		if(destpos!=null){
				//访问mes，根据工作中心查询机台指令和生产订单pk
				JSONObject param1=new JSONObject();
				param1.put("destpos", destpos);
				JSONObject result2=MesServiceCaller.callMesService(session, MES_ACTION_QUERYTASK, param1);
				if(result2!=null){
					String statuscode=result2.getString("statuscode");
					if("0".equals(statuscode)){
						if(result2.containsKey("datas")){
							JSONArray taskarray=result2.getJSONObject("datas").getJSONArray("tasks");
							JSONArray workcenters=result2.getJSONObject("datas").getJSONArray("workcenters");
							JSONArray pk_dmos=new JSONArray();
							for(int i=0;i<taskarray.size();i++){
								JSONObject taskobj=taskarray.getJSONObject(i);
								pk_dmos.add(taskobj.get("pk_dmo"));
							}
							//访问nc，根据生产订单pk查询备料计划子项
							param.put(ConstUtil.TASKTYPE, "getPickItems");
							param.put("pk_dmos", pk_dmos);
							JSONObject result3=TransferService.requestService(param, session);
							if(result3!=null&&result3.containsKey("pickitems")){
								Map<String,JSONObject> wkpk2obj=new HashMap<String,JSONObject>();
								for(int i=0;i<workcenters.size();i++){
									JSONObject obj=workcenters.getJSONObject(i);
									wkpk2obj.put(obj.getString("pk"), obj);
								}
								JSONArray pickitems=result3.getJSONArray("pickitems");
								JSONArray matinfos=result3.getJSONArray("matinfos");
								Map<String,JSONObject> pkdmo2pickitem=new HashMap<String,JSONObject>();
								for(int i=0;i<pickitems.size();i++){
									JSONObject pickitem=pickitems.getJSONObject(i);
									pkdmo2pickitem.put(pickitem.getString("pk_dmo"), pickitem);
								}
								Map<String,JSONObject> matpk2obj=new HashMap<String,JSONObject>();
								for(int i=0;i<matinfos.size();i++){
									JSONObject matinfo=matinfos.getJSONObject(i);
									matpk2obj.put(matinfo.getString("pk"), matinfo);
								}
								JSONArray taskpicks=new JSONArray();
								for(int i = 0;i<taskarray.size();i++){
									JSONObject task=taskarray.getJSONObject(i);
									String pk_dmo = task.getString("pk_dmo");
									JSONObject pickitem=pkdmo2pickitem.get(pk_dmo);
									JSONObject workcenter=wkpk2obj.get(task.get("pk_wk"));
									JSONObject matinfo=matpk2obj.get(pickitem.get("cbmaterialvid"));
									JSONObject taskpick=new JSONObject();
									taskpick.put("selected", false);
									taskpick.put("workcenter", workcenter);
									taskpick.put("matinfo", matinfo);
									taskpick.put("macvbillcode",task.get("vbillcode"));
									if(pickitem.containsKey("vbatchcode")&&pickitem.get("vbatchcode")!=null){
										taskpick.put("matbatch", pickitem.get("vbatchcode"));
									}else{
										taskpick.put("matbatch", "pc");
									}
									taskpick.put("reqNum", pickitem.get("nums"));
									taskpick.put("naccoutnum", pickitem.get("naccoutnum"));
									taskpick.put("starttime", pickitem.get("drequiredate"));
									taskpick.put("status", STATUS_VALUE2OBJ.get(task.get("orderstate")).get("name"));
									taskpick.put("cpickmid", pickitem.get("cpickmid"));
									taskpick.put("hvbillcode", pickitem.get("hvbillcode"));
									taskpick.put("cpickm_bid", pickitem.get("cpickm_bid"));
									taskpick.put("vrowno", pickitem.get("vrowno"));
									taskpicks.add(taskpick);
								}
								result.put("tasks", taskpicks);
							}
						}else{
							throw new Exception("从MES中查不到该站点对应的机台指令");
						}
					}else{
						if(result2.containsKey("errinfo")){
							throw new Exception(result2.getString("errinfo"));
						}else{
							throw new Exception("从MES获取机台指令发生异常");
						}
					}
				}
		}
		return result;
	}
	
	/**
	 * 获取仓库对应的工作中心
	 * @param param
	 * @param session
	 * @return
	 */
	public static JSONObject queryWorkCenterByStordoc(JSONObject param, HttpSession session) throws Exception{
		JSONObject result=new JSONObject();
		
		Object org = param.get(ConstUtil.ORG);
		if(org!=null){
			JSONObject param1=new JSONObject();
			param1.put("pk_org",org);
			JSONObject result1=MesServiceCaller.callMesService(session, MES_ACTION_QUERYWORKCENTER, param1);
			if(result1!=null){
				result = result1;
			}
			return result;
		}
		return result;
	}
	
	/**
	 * 获取站点对应的暂存区
	 * @param param
	 * @param session
	 * @return
	 */
	public static JSONObject queryDestposByWk(JSONObject param, HttpSession session) throws Exception{
		JSONObject result=new JSONObject();
		
		Object position = param.get(ConstUtil.POSITION);
		if(position!=null){
			JSONObject param1=new JSONObject();
			param1.put("pk_position",position);
			JSONObject result1=MesServiceCaller.callMesService(session, MES_ACTION_QUERYDESTPOS, param1);
			if(result1!=null){
				result = result1;
			}
			return result;
		}
		return result;
	}
	
	/**
	 * 获取仓库对应的暂存区集合
	 * @param param
	 * @param session
	 * @return
	 */
	public static JSONObject queryDestposByWarehouse(JSONObject param, HttpSession session) throws Exception{
		JSONObject result=new JSONObject();
		
		Object warehouse = param.get(ConstUtil.WAREHOUSE);
		if(warehouse!=null){
			JSONObject param1=new JSONObject();
			param1.put("warehouse",warehouse);
			JSONObject result1=MesServiceCaller.callMesService(session, MES_ACTION_QUERYDESTPOSBYWH, param1);
			if(result1!=null){
				result = result1;
			}
			return result;
		}
		return result;
	}
}
