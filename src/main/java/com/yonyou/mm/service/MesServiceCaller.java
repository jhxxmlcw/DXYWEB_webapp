package com.yonyou.mm.service;

import javax.servlet.http.HttpSession;

import com.yonyou.mm.connector.ConnectService;
import com.yonyou.mm.params.ParamsEngine;
import com.yonyou.mm.util.ConstUtil;
import com.yonyou.mm.util.EnumMethod;

import net.sf.json.JSONObject;

public class MesServiceCaller {
	public static JSONObject callMesService(HttpSession session,String actiontype,JSONObject param)throws Exception{
		String datasource=(String) session.getAttribute(ConstUtil.MES_DBSOURCE);
		String usercode=(String) session.getAttribute(ConstUtil.MES_USERNAME);
		String password=(String) session.getAttribute(ConstUtil.MES_PASSWORD);
		return callMesService(datasource,usercode,password,actiontype,param);
	}
	public static JSONObject callMesService(String datasource,String usercode,String password,String actiontype,JSONObject param) throws Exception{
		if(datasource==null||datasource.length()==0){
			throw new Exception("mes数据源不能为空");
		}
		if(usercode==null||usercode.length()==0||password==null||password.length()==0){
			throw new Exception("mes用户名密码不能为空");
		}
		JSONObject newparam=new JSONObject();
		newparam.put("actiontype", actiontype);
		newparam.put("datasource", datasource);
		param.put("sysparam_usercode", usercode);
		param.put("sysparam_password", password);
		newparam.put("param", param);
		String serviceurl = ParamsEngine.getXmlConfig().getString("TransferClient.mesip") + ConstUtil.MES_SERVICE_URL;
		//请求后台处理
		JSONObject  result = ConnectService.HttpRequest(serviceurl, EnumMethod.POST.name(), newparam.toString());
		return result;
	}
}
