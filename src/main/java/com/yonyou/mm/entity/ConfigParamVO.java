package com.yonyou.mm.entity;

/**
 * Created by yijx on 2017/3/23.
 */

public class ConfigParamVO {

    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ConfigParamVO(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "'" + this.getName() + "':'" + this.getValue() + "',";
    }

}