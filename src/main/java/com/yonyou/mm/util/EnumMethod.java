package com.yonyou.mm.util;

/**
 * 请求方式枚举类
 * 
 * @date 2016年8月4日 上午9:02:44
 * @author wangjwm
 *
 */
public enum EnumMethod {
	GET, POST;
}
