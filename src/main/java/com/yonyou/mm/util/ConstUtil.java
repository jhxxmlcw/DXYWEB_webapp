/**
 * 
 */
package com.yonyou.mm.util;

/**
 * 字符标记常量
 * 
 * @author suny
 */
public class ConstUtil {

	//交互信息通用key
	public static final String GROUP="pk_gourp"; //集团
	public static final String ORG="pk_org";   //组织
	public static final String USERTYPE="usertype"; //用户类型
	public static final String DBSOURCE="dbname";  //数据源名称
	public static final String USERNAME="username"; //NC用户编码
	public static final String PASSWORD= "password"; //用户密码
	//MES登录信息
	public static final String MES_GROUP="mes_pk_gourp"; //MES集团
	public static final String MES_ORG="mes_pk_org";   //MES组织
	public static final String MES_USERTYPE="mes_usertype"; //MES用户类型
	public static final String MES_DBSOURCE="mes_dbname";  //MES数据源名称
	public static final String MES_USERNAME="mes_username"; //MES用户编码
	public static final String MES_PASSWORD= "mes_password"; //MES用户密码
	//请求NC任务类型
	public static final String TASKTYPE ="tasktype";

	//备料申请交互信息key
	public static final String ROWS="rows"; 	//多行数据
	//表体
	public static final String POSITION = "position";  //仓库站点
	public static final String DESTPOS = "destpos";  //暂存仓
	public static final String PROVPOS = "provpos";  //供应仓
	public static final String PROVPOSTEXT = "provposText";  //供应仓名称
	public static final String MATCODE="matCode";  //物料编码
	public static final String MATBATCH="matBatch";  //物料批次号
	public static final String REQNUM = "reqNum"; //需求数量
	public static final String REQTIME = "reqTime"; //需求时限

	//保存备料申请返回值
	public static final String ROWNO = "rowno";  //行号
	public static final String BILLID="billid"; //备料申请id

	//仓库、站点key
	public static final String WAREHOUSE="warehouse";  //仓库id
	//组织、仓库、站点名称
	public static final String ORGNAME="orgText";  //组织名称
	public static final String WAREHOUSENAME="warehouseText";  //仓库名称
	public static final String POSITIONNAME="positionText";  //站点名称

	//转库单
	public static final String TRANSBILLCODE="transBillCode";  //转库单单据号

	/*
	 * session 中用户ID对应键名
	 */
	public static final String KEY_USERID_IN_SESSION = "UserId";

	/*
	 * session 中用户类型对应键名
	 */
	public static final String KEY_TYPE_IN_SESSION = "type";

	/*
	 * 查询单据数据URL
	 */
	public static final String WORKTASK_URL = "/service/WorkTaskService";
	
	/**
	 * MES服务访问地址
	 */
	public static final String MES_SERVICE_URL = "/service/MesAPP2NCServlet";

}
