package com.yonyou.mm.util;

import net.sf.json.JSONArray;

/**
 * Created by yijx on 2017/3/23.
 */
public class TestData {
    public static String reqPickmData = "[{'editRowNo':0,'billid':" +
            "'BL2017100301001','rowNo':0}," +
            "{'editRowNo':1,'billid':'BL2017100301001','rowNo':1}]";

    public static String reqPickm = "{'billid':'BL2017100301001','rowNo':0,naccoutnum:2000}";

    public static String material = "{'matid':'10017510000000000C5T','matCode':'11020100301001'," +
            "'matname':'焊丝CHW-50C2','matspec':'0.8','mattype':'~~15'," +
            "'matBatch':'1','matunit':'KG'}";

    public static String taskList = "[{'billid': 'BLJH201703100001', 'matcode': '10017510000000000C0N'," +
            "    'workmachine': '1号机台',    'matname': '焊丝CHW-50C2',   'matspec': '0.8',  " +
            "  'mattype': '~~15',  'matbatch': '1',  'matunit':'KG', 'reqNum': '4000', 'starttime': '11:25'},"+
            "  {  'billid': 'BLJH201703100001',  'matcode': '10017510000000000C5T',  'workmachine': '1号机台'," +
            "  'matname': '焊丝CHW-50C3', 'matspec': '0.8', 'mattype': '~~25', 'matbatch': '1',  'reqNum': '5000', " +
            " 'starttime': '11:35', 'matunit':'KG',    }," +
            "{ 'billid': 'BLJH201703100002',  'matcode': '10017510000000000C0N',  'workmachine': '2号机台', " +
            " 'matname': '焊丝CHW-50C2',  'matspec': '0.8', 'mattype': '~~15',  'matbatch': '1', " +
            "'matunit':'KG','reqNum': '4000',        'starttime': '11:30'    }, " +
            " { 'billid': 'BLJH201703100001',  'matcode': '10017510000000000C5T', 'workmachine': '1号机台'," +
            "'matname': '焊丝CHW-50C2', 'matspec': '0.8','matunit':'KG', 'mattype': '~~15', 'matbatch': '1', " +
            "'reqNum': '4000', 'starttime': '11:30'}, " +
            "{  'billid': 'BLJH201703100001', 'matcode': '100155100000000003ZL', 'workmachine': '1号机台', " +
            " 'matname': '焊丝CHW-50C3',  'matspec': '0.8',  'matunit':'KG', 'mattype': '~~25', 'matbatch': '1', " +
            " 'reqNum': '5000',  'starttime': '11:30'    }]";


    public static String reqPickmList = "[{'billid': '10017510000000011C0N','rowno':'0'," +
            "'cinstockid':'线边仓1','reqTime':'11:30','position':'站点3'," +
            "'matname':'材料1材料1'," + "'matspec':'8','mattype':'~~15','matBatch':'1','reqNum':'5000'," +
            "'matunit':'千克'," + "'nacctransnum':'0','status':'未开始'}," +
            "{'billid': '10017510000000011C0N','rowno':'1','reqTime':'11:40','cinstockid':'线边仓1','position':'站点3','matname':'材料1材料1'," +
            "'matspec':'8','mattype':'','matBatch':'2','reqNum':'4000','matunit':'千克'," +
            "'nacctransnum':'0','status':'未开始'}," +
            "{'billid': '10017510000000011C0N','rowno':'1','reqTime':'11:40','cinstockid':'线边仓1','position':'站点3','matname':'材料1'," +
            "'matspec':'','mattype':'','matBatch':'2','reqNum':'4000','matunit':'千克'," +
            "'nacctransnum':'0','status':'未开始'}," +
            "{'billid': '10017510000000011C0M','rowno':'0','reqTime':'11:50','cinstockid':'线边仓2','position':'站点4','matname':'材料2材料2'," +
            "'matspec':'8','mattype':'~~15','matBatch':'1','reqNum':'4500','matunit':'千克'," +
            "'nacctransnum':'0','status':'未开始'}]";

    public static String warehouses = "[{'warehouse':'10017510000000000C6U','warehouseText':'原材料仓1'}," +
            "{'warehouse':'10017510000000000C6V','warehouseText':'线边仓1'}]";

    public static String positions = "[{'position':'10017510000000044C0N','positionText':'站点1'}," +
            "{'position':'10017510000000045C0N','positionText':'站点2'}," +
            "{'position':'10017510000000046C0N','positionText':'站点3'}," +
            "{'position':'10017510000000047C0N','positionText':'站点4'}]";

    public static String orgs = "[{'pk_org':'0001761000000000076T','orgText':'总厂'},{'pk_org':'0001761000000000076J','orgText':'分厂1'}]";

    public static String allocation="{'allocation':['货位1','货位2']}";

    public static String transBill = "[{'billid':'10017510000000011C0N','rowno':'0'," +
            "'transBillCode':'BLJH201703100001','trasnRowNo':'0','truckNum':'3567'}," +
            "{'billid':'10017510000000011C0N','rowno':'1'," +
            "'transBillCode':'BLJH201703100001','trasnRowNo':'1','truckNum':'4467'}]";
}
