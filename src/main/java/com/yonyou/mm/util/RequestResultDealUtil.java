package com.yonyou.mm.util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

public class RequestResultDealUtil {
	public static final String STATUS_SUCCESS="0";
	public static final String STATUS_ERROR="1";
	public static String wrapResult(String statuscode,Exception exception,Object obj){
		JSONObject retobj=new JSONObject();
		if(exception!=null){
			ExceptionUtils4DxyWeb.dealException(exception);
			String errinfo=exception.getMessage();
			if(errinfo!=null){
				retobj.put("errinfo", errinfo);
			}
		}
		if(obj!=null){
			retobj.put("datas", obj);
		}
		if(statuscode==null){
			retobj.put("statuscode", STATUS_SUCCESS);
		}else{
			retobj.put("statuscode", statuscode);
		}
		return retobj.toString();
	}
	public static void returnResult(HttpServletResponse response,String statuscode,Exception exception,Object obj){
		String result=wrapResult(statuscode,exception,obj);
		try {
            PrintWriter pw = response.getWriter();
            pw.write(result);
        } catch (Exception e) {
        	ExceptionUtils4DxyWeb.dealException(e);
        }
	}
}
